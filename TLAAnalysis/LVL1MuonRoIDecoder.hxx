#pragma once

#include <AnaAlgorithm/AnaAlgorithm.h>

class LVL1MuonRoIDecoder : public EL::AnaAlgorithm
{
public:
  LVL1MuonRoIDecoder(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

private:
  //! LVL1 Muon RoI collection to decode
  std::string m_incollection;
  //! Output collection for decoded LVL1 Muon RoI
  std::string m_outcollection;
};
