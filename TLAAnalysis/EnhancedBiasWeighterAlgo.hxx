#pragma once

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <EnhancedBiasWeighter/EnhancedBiasWeighter.h>

class EnhancedBiasWeighterAlgo : public EL::AnaAlgorithm
{
public:
  EnhancedBiasWeighterAlgo(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

private:
  ToolHandle<IEnhancedBiasWeighter> m_enhancedBiasTool = {this, "EnhancedBiasTool"};
};
