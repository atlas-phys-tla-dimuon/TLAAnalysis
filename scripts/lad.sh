#!/bin/bash

if [ "${#}" -lt 2 ]; then
    echo "usage: ${0} joboptions datasetlist [datasetlist ...]"
    exit 1
fi

jo=${1}
datasetlists=${@:2}

for datasetlist in ${datasetlists};
do
    echo "Running over ${datasetlist}"
    for dsname in $(cat ${datasetlist}); do
	echo "  dataset ${dsname}"

	athena.py ${jo} --filesInput=${DATASETPATH}/${dsname}/* -c "from AthenaCommon.AthenaCommonFlags import athenaCommonFlags; athenaCommonFlags.HistOutputs = [\"ANALYSIS:${dsname}.outputs.root\"]"
    done
done
