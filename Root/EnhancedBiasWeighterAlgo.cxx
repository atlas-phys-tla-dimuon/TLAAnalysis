#include "TLAAnalysis/EnhancedBiasWeighterAlgo.hxx"

#include <xAODCore/ShallowCopy.h>

#include <xAODTrigger/MuonRoIContainer.h>


EnhancedBiasWeighterAlgo :: EnhancedBiasWeighterAlgo ( const std::string& name,
					   ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator)
{
  declareProperty ("enhancedBiasTool", m_enhancedBiasTool, "Enhanced Bias Weight Tool");
}

StatusCode EnhancedBiasWeighterAlgo :: initialize()
{
  ANA_CHECK (m_enhancedBiasTool.retrieve());

  return StatusCode::SUCCESS;
}

StatusCode EnhancedBiasWeighterAlgo :: execute()
{
  const xAOD::EventInfo *eventInfo;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  float ebWeight = m_enhancedBiasTool->getEBWeight(eventInfo);
  ATH_MSG_DEBUG("EB Weight is " << ebWeight);

  eventInfo->auxdecor<float>("EBWeight") = ebWeight;
  
  return StatusCode::SUCCESS;
}

StatusCode EnhancedBiasWeighterAlgo :: finalize()
{ return StatusCode::SUCCESS; }
