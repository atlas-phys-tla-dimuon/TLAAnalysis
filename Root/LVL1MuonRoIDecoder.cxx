#include "TLAAnalysis/LVL1MuonRoIDecoder.hxx"

#include <xAODCore/ShallowCopy.h>

#include <xAODTrigger/MuonRoIContainer.h>


LVL1MuonRoIDecoder :: LVL1MuonRoIDecoder ( const std::string& name,
					   ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator)
{
  declareProperty("InCollection" , m_incollection  = "LVL1MuonRoIs",
		  "LVL1 Muon RoI collection to decode");
  declareProperty("OutCollection", m_outcollection = "MyLVL1MuonRoIs",
		  "Output collection for decoded LVL1 Muon RoI");
}

StatusCode LVL1MuonRoIDecoder :: initialize()
{
  ANA_MSG_INFO( "InCollection  = " << m_incollection  );
  ANA_MSG_INFO( "OutCollection = " << m_outcollection );
  return StatusCode::SUCCESS;
}

StatusCode LVL1MuonRoIDecoder :: execute()
{
  const xAOD::MuonRoIContainer *l1muons;
  ANA_CHECK (evtStore()->retrieve (l1muons, m_incollection));

  static const float thrMapBarrel[] = { 4, 6, 8,10,12,14};

  static const float thrMapEndCap[] = { 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,18,20};

  auto shallowCopy = xAOD::shallowCopyContainer (*l1muons);
  std::unique_ptr<xAOD::MuonRoIContainer   > myl1muons (shallowCopy.first);
  std::unique_ptr<xAOD::ShallowAuxContainer> myl1muonsAux (shallowCopy.second);

  static const SG::AuxElement::Decorator< uint8_t > thrNumber( "thrNumber" );
  static const SG::AuxElement::Decorator< int     > source   ( "source"    );

  for (xAOD::MuonRoI* l1muon : *myl1muons)
    {
      // convenience variables
      int     myThrNumber = l1muon->getThrNumber();
      uint8_t mySource    = l1muon->getSource   ();

      // set decoded decorators
      thrNumber(*l1muon) = myThrNumber;
      source   (*l1muon) = mySource   ;

      // calculations
      float myThrValue = (mySource == xAOD::MuonRoI::RoISource::Barrel) ? thrMapBarrel[myThrNumber-1] : thrMapEndCap[myThrNumber-1];
      l1muon->setThrValue(myThrValue*1e3);
    }

  ANA_CHECK (evtStore()->record (myl1muons   .release(), m_outcollection));
  ANA_CHECK (evtStore()->record (myl1muonsAux.release(), m_outcollection+"Aux."));

  return StatusCode::SUCCESS;
}

StatusCode LVL1MuonRoIDecoder :: finalize()
{ return StatusCode::SUCCESS; }
