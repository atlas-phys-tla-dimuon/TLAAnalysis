from . import driver

import ROOT

class Driver(driver.Driver):
  def __init__(self):
    super().__init__()
    self.help='Run your jobs locally.'

  def create(self):
    driver = ROOT.EL.DirectDriver()
    return driver
