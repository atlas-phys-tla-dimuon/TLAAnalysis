import ROOT

import os.path

def parse_filelist(path):
  """ Parse contents of `path` and return as list.
  Each element is a line. Empty lines lines starting with # are skipped.
  """
  filelist=[]
  with open(path, 'r') as f:
    for line in f:
      if line.startswith('#') : continue
      if not line.strip()     : continue
      line = line.strip()
      filelist.append(line)
  return filelist

class SampleManager:
  """Wrapper around `SH::SampleHandler` that can add samples from different sources.
  """
  def __init__(self, sh_all):
    """
    Parameters:
     sh_all (SH::SampleHandler): SampleHandler instance to which all samples are added.
    """
    self.sh_all=sh_all

  def add_filelist(self, fname):
    """Add file listed in `fname`.

    Sample name: Filelist file name, not including extension.

    Metadata is loaded from a file at the same path, but ending in ".config". The
    config file is assumed to have the following format:
    ```
    xsec=sample cross-section in pb
    filteff=sample filter efficiency
    ```
    The right side is a float. Each row is optional.
    """
    # Sample name
    sname='.'.join(os.path.basename(fname).split('.')[:-1]) # input filelist name without extension
    # Read settings
    fcname=os.path.dirname(fname)+'/'+sname+'.config' # replace .txt with .config
    config={}
    if os.path.exists(fcname): # load configuration if it exists
      with open(fcname, 'r') as f:
        for line in f:
          parts=line.strip().split('=')
          if len(parts)!=2: continue
          config[parts[0].strip()]=parts[1].strip()

    # Add sample to sample handler
    ROOT.SH.readFileList(self.sh_all, sname, fname)
    if 'xsec'    in config: sh_all.get(sname).meta().setDouble(ROOT.SH.MetaFields.crossSection    ,float(config['xsec'   ]))
    if 'filteff' in config: sh_all.get(sname).meta().setDouble(ROOT.SH.MetaFields.filterEfficiency,float(config['filteff']))

  def add_rucio(self, dataset, cache=None):
    """Helper function to add a rucio dataset.

    Sample name: Dataset name.

    Parameters:
      dataset (str): name of dataset or path to dataset list
      cache (str): path to local dataset location, use rucio directly if `None`

    Local location of the dataset is specified using a non-empty `cache`
    parameter. The path should contain each dataset as a separate directory
    with input files. The path can also start with "root://" or "eos://" to
    use the corresponding protocol. Local filesystem is assumed by default.
    """
    # Determine location of datasets
    isxrd=cache is not None and cache.startswith('root://')
    iseos=cache is not None and cache.startswith('eos://')

    if isxrd:
      cache=cache[6:]
    if iseos:
      cache=cache[5:]

    # Add datasets to SampleHandler
    if cache is None:
      ROOT.SH.scanRucio(self.sh_all, dataset)
    elif isxrd:
      self.add_xrootd   (f'{cache}/{dataset}')
    elif iseos:
      self.add_eos      (f'{cache}/{dataset}')
    else:
      self.add_directory(f'{cache}/{dataset}')

  def add_directory(self, path):
    """ Add directory at `path` as a sample using `SampleHandler::ScanDir`.
    """
    ROOT.SH.ScanDir().samplePattern('*').scan(self.sh_all,path)

  def add_eos(self, path):
    """ Add contents of an EOS directory using `SH::scanEOS`
    """
    ROOT.SH.ScanDir().samplePattern('*').scanEOS(self.sh_all,path)

  def add_xrootd(self, path):
    """ Add contents of an XROOTD directory using `SH::scanEOS`
    """
    server, path = path.split('//')
    sh_list = ROOT.SH.DiskListXRD(server, path, True)
    ROOT.SH.ScanDir().scan(self.sh_all, sh_list)

  def add_file(self, path):
    """ Add single file as a sample.
    """
    dirname =os.path.dirname (path)
    basename=os.path.basename(path)
    ROOT.SH.ScanDir().samplePattern(basename).scanDir(self.sh_all,dirname)
