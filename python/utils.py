import argparse
import os

import logging
#REMOVE
# logger = logging.getLogger("tla.utils")

def findFrameworkTypeFromList(ASG_framework_list):
  """Find ASG analysis type (e.g. Base, Top) from a given list.
  """
  ASG_framework_types = [ ASGtype for ASGtype in ASG_framework_list if int( os.environ.get('Analysis'+ASGtype+'_SET_UP', 0) ) ]

  if len(ASG_framework_types) == 0:
    return None
  else:
    return ASG_framework_types[0]

class ColoredFormatter(logging.Formatter):
  RESET_SEQ = "\033[0m"
  COLOR_SEQ = "\033[1;%dm"
  BOLD_SEQ = "\033[1m"
  BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)
  #The background is set with 40 plus the number of the color, and the foreground with 30
  #These are the sequences need to get colored ouput
  COLORS = {
    'WARNING': YELLOW,
    'INFO': WHITE,
    'DEBUG': BLUE,
    'CRITICAL': YELLOW,
    'ERROR': RED
  }

  def __init__(self, msg=None, use_color = True):
    if msg is None:
      msg = "[$BOLD%(asctime)s$RESET][%(levelname)-18s]  %(message)s ($BOLD%(filename)s$RESET:%(lineno)d)"
    msg = msg.replace("$RESET", self.RESET_SEQ).replace("$BOLD", self.BOLD_SEQ)
    logging.Formatter.__init__(self, msg)
    self.use_color = use_color

  def format(self, record):
    levelname = record.levelname
    if self.use_color and levelname in self.COLORS:
      levelname_color = self.COLOR_SEQ % (30 + self.COLORS[levelname]) + levelname + self.RESET_SEQ
      record.levelname = levelname_color
    return logging.Formatter.format(self, record)

# Custom logger class with multiple destinations
class ColoredLogger(logging.Logger):
  def __init__(self, name):
    logging.Logger.__init__(self, name, logging.DEBUG)
    color_formatter = ColoredFormatter()
    if '.' in name: return
    console = logging.StreamHandler()
    console.setFormatter(color_formatter)
    self.addHandler(console)
    return

class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter):
  """Custom formatter for the cli.
  """
  pass


def register_driver(parser,name,cli_options,helpStr,baseUsageStr):
  """Register a new driver to an arguments parser.

  Parameters:
    parser: parser to which driver should be added
    name: name that refers to the driver
    cli_options: extra options to add via `register_on_parser`
    helpStr: help string for driver
    baseUsageStr: usage string with `{0}` for location of parser name
  """
  driver = parser.add_parser(name,
                                     help=helpStr,
                                     usage=baseUsageStr.format(name),
                                     formatter_class=lambda prog: CustomFormatter(prog, max_help_position=30))
  register_on_parser(cli_options, driver)
  return driver
  
def register_on_parser(cli_options, parser):
  """Registers the provided dictionary of `cli_options as `argparse.ArgumentParser` object.
  The key of `cli_options` is the name of the argument and can overriden via a `flags`
  property. The key is prepended with "--".
  The value is a dictionary of properties passed to `add_argument` as kwargs.
  """
  for optName, optConfig in cli_options.items():
    # no flags specified? that's fine, use '--{optName}' as default
    flags = optConfig.pop("flags", ["--{0:s}".format(optName)])
    parser.add_argument(*flags, **optConfig)

def update_clioption_defaults(argdict, newvalues):
  """Update the default fields of an argument definition dictionary from cli-options.

  Keyword arguments:
  argdict -- reference to the argument definitions
  newvalues -- dictionary with the argument name as key and new default value as value
  """

  for key,value in newvalues.items():
    if key in argdict: argdict[key]['default']=value
