class Driver:
  def __init__(self):
    self.help=None

    self.cli_options={
      "optSubmitFlags": {
        "metavar": "",
        "type": str,
        "required": False,
        "default": None,
        "help": "the name of the option for supplying extra submit parameters to batch systems",
      },
      "optEventsPerWorker": {
        "metavar": "",
        "type": float,
        "required": False,
        "default": None,
        "help": "the name of the option for selecting the number of events per batch job.  (only BatchDriver and derived drivers). warning: this option will be ignored unless you have called SH::scanNEvents first.",
      },
      "optFilesPerWorker": {
        "metavar": "",
        "type": float,
        "required": False,
        "default": None,
        "help": "the name of the option for selecting the number of files per batch job.  (only BatchDriver and derived drivers).",
      },
      "optDisableMetrics": {
        "metavar": "",
        "type": int,
        "required": False,
        "default": None,
        "help": "the option to turn off collection of performance data",
      },
      "optPrintPerFileStats": {
        "metavar": "",
        "type": int,
        "required": False,
        "default": None,
        "help": "the option to turn on printing of i/o statistics at the end of each file. warning: this is not supported for all drivers.",
      },
      "optRemoveSubmitDir": {
        "metavar": "",
        "type": int,
        "required": False,
        "default": None,
        "help": "the name of the option for overwriting the submission directory.  if you set this to a non-zero value it will remove any existing submit-directory before tryingto create a new one. You can also use -f/--force as well in xAH_run.py.",
      },
      "optBatchSharedFileSystem": {
        "type": bool,
        "required": False,
        "default": False,
        "help": "enable to signify whether your batch driver is running on a shared filesystem",
      },
      "optBatchWait": {
        "action": "store_true",
        "required": False,
        "help": "submit using the submit() command. This causes the code to wait until all jobs are finished and then merge all of the outputs automatically",
      },
      "optBatchShellInit": {
        "metavar": "",
        "type": str,
        "required": False,
        "default": "",
        "help": "extra code to execute on each batch node before starting EventLoop",
      },
    }

    def create(self):
      return None
